module bottin

go 1.13

require (
	github.com/go-ldap/ldap/v3 v3.3.0
	github.com/google/uuid v1.1.1
	github.com/hashicorp/consul/api v1.3.0
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9 // indirect
)
