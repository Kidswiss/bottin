package main

import (
	ldap "github.com/go-ldap/ldap/v3"
)

func (inst *instance) Add_Request(dn string, attributes map[string][]string) error {
	//Create the AddRequest
	req := ldap.NewAddRequest(dn, nil)
	for key, value := range attributes {
		req.Attribute(key, value)
	}

	//Send the request
	err := inst.logging.Add(req)
	return err

}

//Use enum to select Replace,Delete,Modify
func (inst *instance) Modify_Request(dn string, add_attributes, delete_attributes, replace_attributes map[string][]string) error {
	modifyReq := ldap.NewModifyRequest(dn, nil)

	for key, value := range add_attributes {
		modifyReq.Add(key, value)
	}

	for key, value := range delete_attributes {
		modifyReq.Delete(key, value)
	}

	for key, value := range replace_attributes {
		modifyReq.Replace(key, value)
	}

	err := inst.logging.Modify(modifyReq)
	return err
}

func (inst *instance) Delete_Request(dn string) error {
	del := ldap.NewDelRequest(dn, nil)

	err := inst.logging.Del(del)
	return err
}

func (inst *instance) Search_Request(dn, filter string, name_attributes []string) (*ldap.SearchResult, error) {
	searchReq := ldap.NewSearchRequest(
		dn,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		filter,
		name_attributes,
		nil,
	)

	res, err := inst.logging.Search(searchReq)
	logging.Debugf("Search Request made with: dn: %s, filter: %s, attributes: %s. \n", dn, filter, name_attributes)
	return res, err
}
