package main

import (
	"testing"
)

func TestAddThenDelete(t *testing.T) {
	t.Parallel()
	//SetUp - Create Users and Groups
	inst, err := Init()
	if err != nil {
		t.Error(err)
	}

	//TearDown - Delete all the users and groups created
	err = inst.Clean()
	if err != nil {
		t.Error(err)
	}
}

func TestConfirmAddAttributes(t *testing.T) {
	t.Parallel()
	//SetUp - Create Users and Groups
	inst, err := Init()
	if err != nil {
		t.Error(err)
	}

	//Test search_attribute to confirm the Add
	if ok, err := inst.CompareOurDataWithConsul(); !ok {
		t.Error(err)
	}
	//TearDown - Delete all the users and groups created
	err = inst.Clean()
	if err != nil {
		t.Error(err)
	}
}

//Modifyrequest Test
func TestModifyRequest(t *testing.T) {
	t.Parallel()
	//SetUp - Create Users and Groups
	inst, err := Init()
	if err != nil {
		t.Error(err)
	}

	//Test modify all data (groups and users)
	err = inst.ModifyRandomAllData()
	if err != nil {
		t.Error(err)
	}

	//TearDown - Delete all the users and groups created
	err = inst.Clean()
	if err != nil {
		t.Error(err)
	}
}

func TestModifyRequestAndCheck(t *testing.T) {
	t.Parallel()
	//SetUp - Create Users and Groups
	inst, err := Init()
	if err != nil {
		t.Error(err)
	}

	//Test modify all data (groups and users)
	err = inst.ModifyRandomAllData()
	if err != nil {
		t.Error(err)
	}

	//Check if the data was modify on Consul
	if ok, err := inst.CompareOurDataWithConsul(); !ok {
		t.Error(err)
	}

	//TearDown - Delete all the users and groups created
	err = inst.Clean()
	if err != nil {
		t.Error(err)
	}
}

func TestAddUserInGroup(t *testing.T) {
	t.Parallel()
	//SetUp - Create Users and Groups
	inst, err := Init()
	if err != nil {
		t.Error(err)
	}

	//Add users in group
	err = inst.AddAllUsersInGroup()
	if err != nil {
		t.Error(err)
	}

	//TearDown - Delete all the users and groups created
	err = inst.Clean()
	if err != nil {
		t.Error(err)
	}
}

func TestDeleteGroupsAfterAddedUsers(t *testing.T) {
	t.Parallel()
	//SetUp - Create Users and Groups
	inst, err := Init()
	if err != nil {
		t.Error(err)
	}

	//Add users in group
	err = inst.AddAllUsersInGroup()
	if err != nil {
		t.Error(err)
	}

	//Delete the half groups
	number := len(inst.dataGroups) / 2
	err = inst.clean(inst.dataGroups[0:number])
	if err != nil {
		t.Error(err)
	}
	inst.dataGroups = inst.dataGroups[number:len(inst.dataGroups)]

	//Check all the groups in memberOf exist
	ok, err := inst.CheckMemberOf()
	if err != nil {
		t.Error(err)
	}
	if !ok {
		t.Errorf("Found group in memberOf that isn't in Consul.")
	}

	//TearDown - Delete all the users and groups created
	err = inst.Clean()
	if err != nil {
		t.Error(err)
	}
}

//Example of paralellism Test
func TestPrincipal(t *testing.T) {
	t.Run("A=Add and delete", TestAddThenDelete)
	t.Run("A=Modify", TestModifyRequest)
	if !testing.Short() {
		t.Run("B=Add attributes", TestConfirmAddAttributes)
		t.Run("B=Modify and check", TestModifyRequestAndCheck)
		t.Run("C=Add user in group", TestAddUserInGroup)
		t.Run("C=Delete group", TestDeleteGroupsAfterAddedUsers)
	}
}
